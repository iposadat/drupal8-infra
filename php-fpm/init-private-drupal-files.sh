#!/bin/bash

set -e

# Symlink to files
echo "-> Removing files folder to perform symlink..."
rm -rf /var/www/html/default/web/sites/$SITE_FOLDER/files

echo "-> Creating symlink..."
ln -sf /private/files /var/www/html/default/web/sites/$SITE_FOLDER/

ls -al /var/www/html/default/web/sites/$SITE_FOLDER/

echo "-> [DONE]"