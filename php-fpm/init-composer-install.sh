#!/bin/bash

set -e

composer create-project drupal-composer/drupal-project:8.x-dev $SITE_FOLDER --no-interaction --no-install

cd /var/www/html/$SITE_FOLDER
composer install

echo "-> [DONE]"